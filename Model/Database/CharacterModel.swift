//
//  CharacterModel.swift
//  marvel
//
//  Created by Paulo Rodrigues on 29/09/16.
//  Copyright © 2016 Riabox Software. All rights reserved.
//

import Foundation

class CharacterModel {
    
    static let database = SQLiteManager.sharedInstance.database!
    
    static func listAll() -> [Character] {
        if database.open() == false {
            print("Unable to open database")
            return []
        }
        
        var characters: [Character] = []
        
        do {
            let resultSet = try database.executeQuery("SELECT * FROM characters ORDER BY created", values: nil)
            
            while resultSet.next() {
                let character = Character()
                
                character.id = Int(resultSet.int(forColumn: "id"))
                character.name = resultSet.string(forColumn: "name")
                
                if resultSet.columnIsNull("description") == false {
                    character.description = resultSet.string(forColumn: "description")
                }
                
                if resultSet.columnIsNull("thumbnail") == false {
                    character.thumbnail = resultSet.string(forColumn: "thumbnail")
                }
                
                character.created = Functions.dateFromString(resultSet.string(forColumn: "created"))
                
                characters.append(character)
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        database.close()
        
        return characters
    }
    
    static func findById(id: Int) -> Character? {
        if database.open() == false {
            print("Unable to open database")
            return nil
        }
        
        var character: Character?
        
        do {
            let resultSet = try database.executeQuery("SELECT * FROM characters WHERE id = ? ", values: [id])
            
            if resultSet.next() {
                character = Character()
                
                character!.id = Int(resultSet.int(forColumn: "id"))
                character!.name = resultSet.string(forColumn: "name")
                
                if resultSet.columnIsNull("description") == false {
                    character!.description = resultSet.string(forColumn: "description")
                }
                
                if resultSet.columnIsNull("thumbnail") == false {
                    character!.thumbnail = resultSet.string(forColumn: "thumbnail")
                }
                
                character!.created = Functions.dateFromString(resultSet.string(forColumn: "created"))
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        database.close()
        
        return character
    }
    
    static func save(_ character: Character) -> Bool {
        if database.open() == false {
            print("Unable to open database")
            return false
        }
        
        var success: Bool = false
        
        database.beginTransaction()
        
        do {
            let description = character.description
            let thumbnail = character.thumbnail
            let created = Functions.stringFromDate(Date())
            
            let data: [Any] = [character.id, character.name, description != nil ? description! : NSNull(), thumbnail != nil ? character.thumbnail! : NSNull(), created != nil ? created : NSNull()]
            
            try database.executeUpdate("INSERT OR REPLACE INTO characters (id, name, description, thumbnail, created) values (?, ?, ?, ?, ?)", values: data)
            
            success = true
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        if success {
            success = database.commit()
        }
        
        database.close()
        
        return success
    }
    
    static func delete(_ character: Character) -> Bool {
        if database.open() == false {
            print("Unable to open database")
            return false
        }
        
        var success: Bool = false
        
        database.beginTransaction()
        
        do {
            try database.executeUpdate("DELETE FROM characters WHERE id = ?", values: [character.id])
            
            success = true
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        if success {
            success = database.commit()
        }
        
        database.close()
        
        return success
    }
    
}
