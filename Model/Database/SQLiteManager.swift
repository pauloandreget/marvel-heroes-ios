//
//  SQLiteManager.swift
//  marvel
//
//  Created by Paulo Rodrigues on 29/09/16.
//  Copyright © 2016 Riabox Software. All rights reserved.
//

import Foundation
import FMDB

class SQLiteManager: NSObject {
    
    let currentSchemaVersion = 1
    
    var database: FMDatabase?
    
    class var sharedInstance: SQLiteManager {
        struct Static {
            static let instance = SQLiteManager()
        }
        
        return Static.instance
    }
    
    override init() {
        super.init()
        
        if let dbPath: String = getPath() {
            database = FMDatabase(path: dbPath)
            
            if database != nil {
                let schemaVersion = databaseSchemaVersion(database!)
                
                if schemaVersion == 0 {
                    // New installation, create tables
                    createTables(database!)
                    // Set schema version
                    setDatabaseSchemaVersion()
                    
                    print("create tables, schema version \(currentSchemaVersion)")
                } else {
                    if schemaVersion < currentSchemaVersion {
                        // Execute migration queries
                        if migrateDatabase(database!) {
                            print("database migration to version \(currentSchemaVersion)")
                        } else {
                            print("database migration failed")
                        }
                    }
                }
            }
        }
    }
    
    func getPath() -> String? {
        let documents = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        
        let fileURL = documents.appendingPathComponent("marvel.sqlite")
        
        return fileURL.path
    }
    
    func createTables(_ db: FMDatabase) {
        if db.open() {
            do {
                try db.executeUpdate("CREATE TABLE characters (id INTEGER PRIMARY KEY, name TEXT, description TEXT, thumbnail TEXT, created TEXT)", values: nil)
            } catch let error as NSError {
                assert(false, error.localizedDescription)
            }
        }
        
        db.close()
    }
    
    func databaseSchemaVersion(_ db: FMDatabase) -> Int {
        var version = 0
        
        if db.open() {
            do {
                let resultSet = try db.executeQuery("PRAGMA user_version", values: nil)
                
                if resultSet.next() {
                    version = Int(resultSet.int(forColumnIndex: 0))
                }
            } catch let error as NSError {
                assert(false, error.localizedDescription)
            }
        }
        
        db.close()
        
        return version
    }
    
    func setDatabaseSchemaVersion() {
        var db: OpaquePointer? = nil
        
        if (sqlite3_open(getPath()!, &db) == SQLITE_OK) {
            sqlite3_exec(db, "PRAGMA user_version = \(currentSchemaVersion)", nil, nil, nil)
        } else {
            sqlite3_errmsg(db)
        }
        
        sqlite3_close(db)
    }
    
    func migrateDatabase(_ db: FMDatabase) -> Bool {
        var success = false
        
        if let file = Bundle.main.path(forResource: "\(currentSchemaVersion)", ofType: "sql", inDirectory: "Migration") {
            var query = ""
            
            do {
                query = try NSString(contentsOfFile: file, encoding: String.Encoding.utf8.rawValue) as String
                
                if db.open() {
                    do {
                        try db.executeUpdate(query, values: nil)
                        
                        // Set schema version
                        setDatabaseSchemaVersion()
                        
                        success = true
                    } catch let error as NSError {
                        assert(false, error.localizedDescription)
                    }
                }
                
                db.close()
            } catch let error as NSError {
                assert(false, error.localizedDescription)
            }
        }
        
        return success
    }
    
}
