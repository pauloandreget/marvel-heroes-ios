//
//  GenericResponse.swift
//  marvel
//
//  Created by Paulo Rodrigues on 28/09/16.
//  Copyright © 2016 Riabox Software. All rights reserved.
//

import Foundation
import ObjectMapper

class GenericResponse<T: Mappable>: Mappable {
    
    var code: Int = 0
    var status: String = ""
    var data: ContainerResponse<T>?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        status <- map["status"]
        data <- map["data"]
    }
    
}
