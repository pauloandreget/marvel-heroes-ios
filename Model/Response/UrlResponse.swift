//
//  UrlResponse.swift
//  marvel
//
//  Created by Paulo Rodrigues on 29/09/16.
//  Copyright © 2016 Riabox Software. All rights reserved.
//

import Foundation
import ObjectMapper

class UrlResponse: Mappable {
    
    var type: String?
    var url: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        type <- map["type"]
        url <- map["url"]
    }
    
}
