//
//  ThumbnailResponse.swift
//  marvel
//
//  Created by Paulo Rodrigues on 29/09/16.
//  Copyright © 2016 Riabox Software. All rights reserved.
//

import Foundation
import ObjectMapper

class ThumbnailResponse: Mappable {
    
    var path: String?
    var type: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        path <- map["path"]
        type <- map["extension"]
    }
    
}
