//
//  CharacterResponse.swift
//  marvel
//
//  Created by Paulo Rodrigues on 28/09/16.
//  Copyright © 2016 Riabox Software. All rights reserved.
//

import Foundation
import ObjectMapper

class CharacterResponse: Mappable {
    
    var id: Int?
    var name: String?
    var description: String?
    var thumbnail: ThumbnailResponse?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        description <- map["description"]
        thumbnail <- map["thumbnail"]
    }
    
}
