//
//  EventResponse.swift
//  marvel
//
//  Created by Paulo Rodrigues on 28/09/16.
//  Copyright © 2016 Riabox Software. All rights reserved.
//

import Foundation
import ObjectMapper

class EventResponse: Mappable {
    
    var id: Int?
    var title: String?
    var description: String?
    var thumbnail: ThumbnailResponse?
    var urls: [UrlResponse]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        description <- map["description"]
        thumbnail <- map["thumbnail"]
        urls <- map["urls"]
    }
    
}
