//
//  Character.swift
//  marvel
//
//  Created by Paulo Rodrigues on 29/09/16.
//  Copyright © 2016 Riabox Software. All rights reserved.
//

import Foundation

class Character {
    
    var id: Int = 0
    var name: String = ""
    var description: String?
    var thumbnail: String?
    var created: Date?
    
    init() {
        
    }
    
    init(id: Int, name: String, description: String? = nil, thumbnail: String? = nil) {
        self.id = id
        self.name = name
        self.description = description
        self.thumbnail = thumbnail
    }

}
