//
//  CharacterWs.swift
//  marvel
//
//  Created by Paulo Rodrigues on 28/09/16.
//  Copyright © 2016 Riabox Software. All rights reserved.
//

import Foundation
import ObjectMapper

class CharacterWs: WsClient {
    
    func fetchAll(nameStartsWith: String = "", orderBy: String = "name", limit: Int = 20, offset: Int = 0) {
        var params: [String : Any] = [:]
        
        if nameStartsWith.characters.count > 0 {
            params["nameStartsWith"] = nameStartsWith
        }
        
        params["orderBy"] = orderBy
        params["limit"] = limit
        params["offset"] = offset
        
        callGet("v1/public/characters", parameters: params, type: GenericResponse<CharacterResponse>.self)
    }
    
    func fetchOne(characterId: Int) {
        callGet("v1/public/characters/\(characterId)", parameters: nil, type: GenericResponse<CharacterResponse>.self)
    }
    
    func fetchEvents(characterId: Int, limit: Int = 20, offset: Int = 0) {
        let params: [String : Any] = ["limit": limit, "offset": offset]
        
        callGet("v1/public/characters/\(characterId)/events", parameters: params, type: GenericResponse<EventResponse>.self)
    }
    
}
