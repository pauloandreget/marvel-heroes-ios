//
//  WsClient.swift
//  marvel
//
//  Created by Paulo Rodrigues on 28/09/16.
//  Copyright © 2016 Riabox Software. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class WsClient: NSObject {
    var successListener: ((_ dataResponse: Mappable?) -> Void)!
    var failureListener: ((_ error: NSError?) -> Void)!
    
    var manager: SessionManager?
    
    override init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = TIMEOUT
        configuration.timeoutIntervalForResource = TIMEOUT
        
        manager = Alamofire.SessionManager(configuration: configuration)
    }
    
    convenience init(success: @escaping (_ data: Mappable?) -> (), failure: @escaping (_ error: NSError?) -> ()) {
        self.init()
        
        successListener = success
        failureListener = failure
    }
    
    func callGet<T: Mappable>(_ destination: String, parameters: Any?, type: T.Type) {
        request(destination, method: .get, parameters: parameters, type: type)
    }
    
    func request<T: Mappable>(_ destination: String, method: HTTPMethod, parameters: Any?, type: T.Type) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let ts: TimeInterval = NSDate().timeIntervalSince1970
        
        var params: [String : Any] = ["ts": Int(ts), "apikey": PUBLIC_KEY, "hash": Functions.md5("\(Int(ts))\(PRIVATE_KEY)\(PUBLIC_KEY)")]
        
        if let objectParams = parameters as? [String : Any] {
            objectParams.forEach { params.updateValue($1, forKey: $0) }
        }
        
        let url: String = String(format: "%@%@", MARVEL_WS, destination)
        
        manager?.request(url, method: method, parameters: params)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseObject { (response: DataResponse<T>) in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                switch response.result {
                case .success:
                    self.notifySuccess(response.result.value)
                case .failure(let error):
                    self.notifyFailure(error as NSError?)
                }
                
                self.manager?.session.invalidateAndCancel()
        }
    }
    
    func notifySuccess(_ dataResponse: Mappable?) {
        if successListener != nil {
            successListener(dataResponse)
        }
    }
    
    func notifyFailure(_ error: NSError?) {
        if failureListener != nil {
            failureListener(error)
        }
    }
}
