//
//  MainViewController.swift
//  marvel
//
//  Created by Paulo Rodrigues on 28/09/16.
//  Copyright © 2016 Riabox Software. All rights reserved.
//

import UIKit
import ObjectMapper
import FontAwesome_swift
import AlamofireImage

class MainViewController: UITableViewController, UITabBarDelegate, UISearchBarDelegate {
    
    let downloader = ImageDownloader(
        configuration: ImageDownloader.defaultURLSessionConfiguration(),
        downloadPrioritization: .fifo,
        maximumActiveDownloads: 6,
        imageCache: AutoPurgingImageCache()
    )
    
    var favorites: [Character] = []
    var characters: [CharacterResponse] = []
    
    var loadingIndicator: UIActivityIndicatorView?
    var searchBar: UISearchBar!
    var iconFavOff: UIImage!
    var iconFavOn: UIImage!
    
    var isLoading: Bool = false
    var hasMore: Bool = true
    var filter: String = ""
    var sort: String = "name"
    var offset: Int = 0
    
    let limit: Int = 20

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = NSLocalizedString("Marvel Heroes", comment: "")

        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        
        let iconRefresh: UIImage = UIImage.fontAwesomeIconWithName(.Refresh, textColor: UIColor.white, size: CGSize(width: 30.0, height: 30.0))
        let btnRefresh = UIBarButtonItem(image: iconRefresh, style: .plain, target: self, action: #selector(MainViewController.refresh(_:)))
        
        navigationItem.leftBarButtonItem = btnRefresh
        
        let iconSort: UIImage = UIImage.fontAwesomeIconWithName(.Sort, textColor: UIColor.white, size: CGSize(width: 30.0, height: 30.0))
        let btnSort = UIBarButtonItem(image: iconSort, style: .plain, target: self, action: #selector(MainViewController.sortList(_:)))
        
        navigationItem.rightBarButtonItem = btnSort

        loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 0.0, y: 0.0, width: tableView.frame.width, height: 44.0))
        loadingIndicator?.activityIndicatorViewStyle = .gray
        loadingIndicator?.hidesWhenStopped = true
        loadingIndicator?.stopAnimating()
        
        searchBar = UISearchBar(frame: CGRect(x: 0.0, y: 0.0, width: 100.0, height: 30.0))
        searchBar.delegate = self
        searchBar.placeholder = NSLocalizedString("Search", comment: "")
        searchBar.barTintColor = UIColor(red: 45/255.0, green: 46/255.0, blue: 47/255.0, alpha: 1.0)
        
        navigationItem.titleView = searchBar
        
        self.definesPresentationContext = true
        
        iconFavOff = UIImage.fontAwesomeIconWithName(.Star, textColor: UIColor(red: 230/255.0, green: 234/255.0, blue: 237/255.0, alpha: 1.0), size: CGSize(width: 30.0, height: 30.0))
        iconFavOn = UIImage.fontAwesomeIconWithName(.Star, textColor: UIColor(red: 255/255.0, green: 203/255.0, blue: 43/255.0, alpha: 1.0), size: CGSize(width: 30.0, height: 30.0))
        
        favorites = CharacterModel.listAll()
        
        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.reloadFavorites(_:)), name: NSNotification.Name(rawValue: "ReloadFavorites"), object: nil)
    }
    
    func reloadFavorites(_ notification: Notification?) {
        favorites = CharacterModel.listAll()
        tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if characters.count == 0 {
            Functions.showIndeterminateHUD()
            loadMore()
        }
    }
    
    func refresh(_ sender: UIBarButtonItem?) {
        Functions.showIndeterminateHUD()
        
        offset = 0
        
        loadMore()
    }
    
    func sortList(_ sender: UIBarButtonItem) {
        let actionSheet = UIAlertController(title: nil, message: NSLocalizedString("Alphabet Order", comment: ""), preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Ascending", comment: ""), style: .default, handler: sortAscending))
        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Descending", comment: ""), style: .default, handler: sortDescending))
        
        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    func sortAscending(_ alert: UIAlertAction) {
        sort = "name"
        refresh(nil)
    }
    
    func sortDescending(_ alert: UIAlertAction) {
        sort = "-name"
        refresh(nil)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text != filter {
            sort = "name"
            filter = searchBar.text!
            refresh(nil)
        }
        
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text != filter {
            sort = "name"
            filter = ""
            refresh(nil)
        }
        
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func loadMore() {
        isLoading = true
        
        let characterWs: CharacterWs = CharacterWs(success: responseSuccess, failure: responseFailure)
        characterWs.fetchAll(nameStartsWith: filter, orderBy: sort, limit: limit, offset: offset)
    }
    
    func responseSuccess(_ data: Mappable?) {
        isLoading = false
        
        loadingIndicator?.stopAnimating()
        
        Functions.hideIndeterminateHUD()
        
        if let response = data as? GenericResponse<CharacterResponse> {
            if response.code == 200 {
                if let data = response.data {
                    if offset == 0 {
                        characters.removeAll()
                    }
                    
                    characters.append(contentsOf: data.results!)
                    
                    if (offset + 1) * limit >= data.total! {
                        hasMore = false
                    }
                }
            } else {
                Functions.showAlert(NSLocalizedString("Alert", comment: ""), message: response.status, buttonTitle: NSLocalizedString("Ok", comment: ""), viewController: self)
            }
        }
        
        UIView.performWithoutAnimation {
            tableView.reloadSections(IndexSet(integer: 1), with: .none)
        }
        
        if characters.count == 0 {
            Functions.showAlert(NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("No characters found.", comment: ""), buttonTitle: NSLocalizedString("Ok", comment: ""), viewController: self)
        }
    }
    
    func responseFailure(_ error: NSError?) {
        isLoading = false
        
        if offset > 0 {
            offset -= limit
        }
        
        loadingIndicator?.stopAnimating()
        
        Functions.hideIndeterminateHUD()
        
        if error != nil {
            Functions.showAlert(NSLocalizedString("Alert", comment: ""), message: error!.localizedDescription, buttonTitle: NSLocalizedString("Ok", comment: ""), viewController: self)
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if maximumOffset - currentOffset > 1.0 {
            return
        }
        
        if hasMore && isLoading == false {
            loadingIndicator?.startAnimating()
            
            offset += limit
            
            loadMore()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var titleHeader: String?
        
        if section == 0 && favorites.count > 0 {
            titleHeader = NSLocalizedString("Favorites", comment: "")
        } else if section == 1 && characters.count > 0 {
            titleHeader = NSLocalizedString("Characters", comment: "")
        }
        
        return titleHeader
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? favorites.count : characters.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier")
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "reuseIdentifier")
        }
        
        cell.imageView?.image = nil
        
        if indexPath.section == 0 {
            let character: Character = favorites[indexPath.row]
            
            cell.textLabel?.text = character.name
            cell.textLabel?.font = UIFont(name: "TrebuchetMS", size: 20.0)
            
            cell.accessoryView = nil
            
            if character.thumbnail != nil {
                let urlRequest = URLRequest(url: URL(string: character.thumbnail!)!)
                let filter = AspectScaledToFillSizeCircleFilter(size: CGSize(width: 40.0, height: 40.0))
                
                downloader.download(urlRequest, filter: filter) { response in
                    if let image = response.result.value {
                        cell.imageView?.image = image
                        cell.layoutSubviews()
                    }
                }
            }
        } else {
            let character: CharacterResponse = characters[indexPath.row]
            
            cell.textLabel?.text = character.name
            cell.textLabel?.font = UIFont(name: "TrebuchetMS", size: 20.0)
            
            let buttonFav: UIButton = UIButton(frame: CGRect(x: 0.0, y: 0.0, width: 30.0, height: 30.0))
            
            buttonFav.tag = indexPath.row
            
            buttonFav.setImage(iconFavOff, for: UIControlState.normal)
            buttonFav.setImage(iconFavOn, for: UIControlState.selected)
            
            buttonFav.addTarget(self, action: #selector(MainViewController.toggleFavorite(_:)), for: .touchUpInside)
            
            if favorites.contains(where: { $0.id == character.id }) {
                buttonFav.isSelected = true
            } else {
                buttonFav.isSelected = false
            }
            
            cell.accessoryView = buttonFav
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 0 ? UITableViewAutomaticDimension : 44.0
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return section == 0 ? nil : loadingIndicator
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailsView = DetailsViewController(nibName: "DetailsViewController", bundle: nil)
        
        if indexPath.section == 0 {
            detailsView.character = favorites[indexPath.row]
        } else {
            detailsView.character = entityFromDto(characters[indexPath.row])
        }
        
        navigationController?.pushViewController(detailsView, animated: true)
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.section == 0
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let character: Character = favorites[indexPath.row]
            
            if CharacterModel.delete(character) {
                favorites.remove(at: indexPath.row)
                
                tableView.beginUpdates()
                
                if favorites.count == 0 {
                    tableView.reloadSections(IndexSet(integer: 0), with: .fade)
                } else {
                    tableView.deleteRows(at: [indexPath], with: .fade)
                }
                
                tableView.endUpdates()
                
                if let index = characters.index(where: { $0.id == character.id }) {
                    if let cell = tableView.cellForRow(at: IndexPath(row: index, section: 1)) {
                        if let buttonFav = cell.accessoryView as? UIButton {
                            buttonFav.isSelected = false
                        }
                    }
                }
            }
        }
    }
    
    func toggleFavorite(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
        } else {
            sender.isSelected = true
        }
        
        let characterDto: CharacterResponse = characters[sender.tag]
        
        let list = favorites.filter { (favorite) -> Bool in
            return favorite.id == characterDto.id
        }
        
        if list.count == 0 {
            // add
            let character: Character = entityFromDto(characterDto)
            
            if CharacterModel.save(character) {
                favorites.append(character)
                
                var currentOffset = tableView.contentOffset
                currentOffset.y += 50.0
                
                UIView.performWithoutAnimation {
                    tableView.insertRows(at: [IndexPath(row: favorites.count - 1, section: 0)], with: .none)
                }
                
                tableView.setContentOffset(currentOffset, animated: false)
            }
        } else {
            // remove
            if let index = favorites.index(where: { $0 === list.first }) {
                if CharacterModel.delete(favorites[index]) {
                    favorites.remove(at: index)
                    
                    var currentOffset = tableView.contentOffset
                    currentOffset.y -= 50.0
                    
                    UIView.performWithoutAnimation {
                        if favorites.count == 0 {
                            tableView.reloadSections(IndexSet(integer: 0), with: .none)
                        } else {
                            tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .none)
                        }
                    }
                    
                    tableView.setContentOffset(currentOffset, animated: false)
                }
            }
        }
    }
    
    private func entityFromDto(_ dto: CharacterResponse) -> Character {
        var thumbnail: String?
        
        if dto.thumbnail != nil {
            if let path = dto.thumbnail!.path, let type = dto.thumbnail!.type {
                thumbnail = "\(path).\(type)".replacingOccurrences(of: "http://", with: "https://")
            }
        }
        
        var description: String?
        
        if dto.description != "" {
            description = dto.description
        }
        
        return Character(id: dto.id!, name: dto.name!, description: description, thumbnail: thumbnail)
    }

}
