//
//  DetailsViewController.swift
//  marvel
//
//  Created by Paulo Rodrigues on 29/09/16.
//  Copyright © 2016 Riabox Software. All rights reserved.
//

import UIKit
import AlamofireImage
import ObjectMapper

class DetailsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var characterView: UIView!
    @IBOutlet weak var characterViewHeight: NSLayoutConstraint!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var characterName: UILabel!
    @IBOutlet weak var characterDescription: UILabel!
    @IBOutlet weak var characterDescriptionHeight: NSLayoutConstraint!
    
    let downloader = ImageDownloader(
        configuration: ImageDownloader.defaultURLSessionConfiguration(),
        downloadPrioritization: .fifo,
        maximumActiveDownloads: 6,
        imageCache: AutoPurgingImageCache()
    )
    
    var loadingIndicator: UIActivityIndicatorView?
    
    var isLoading: Bool = true
    var hasMore: Bool = true
    var favorited: Bool = false
    var offset: Int = 0
    
    let limit: Int = 20
    let widthItem = (UIScreen.main.bounds.size.width - 4) / 3
    
    var character: Character!
    
    var events: [EventResponse] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = character.name
        
        favorited = CharacterModel.findById(id: character.id) != nil
        
        let iconFav: UIImage = UIImage.fontAwesomeIconWithName(.Star, textColor: UIColor.white, size: CGSize(width: 30.0, height: 30.0))
        let btnFav = UIBarButtonItem(image: iconFav, style: .plain, target: self, action: #selector(DetailsViewController.toggleFavorite(_:)))
        
        navigationItem.rightBarButtonItem = btnFav
        
        if favorited {
            navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 255/255.0, green: 203/255.0, blue: 43/255.0, alpha: 1.0)
        }
        
        collectionView.register(UINib(nibName: "EventViewCell", bundle: nil), forCellWithReuseIdentifier: "cellIdentifier")
        
        collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerView")
        collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "footerView")
        
        if let flowLayout: UICollectionViewFlowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.sectionInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
            flowLayout.itemSize = CGSize(width: widthItem, height: widthItem)
            flowLayout.minimumInteritemSpacing = 2
            flowLayout.minimumLineSpacing = 2
        }
        
        let widthScreen = UIScreen.main.bounds.size.width
        
        loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 0.0, y: 0.0, width: widthScreen, height: 44.0))
        loadingIndicator?.activityIndicatorViewStyle = .gray
        loadingIndicator?.hidesWhenStopped = true
        loadingIndicator?.startAnimating()
        
        characterViewHeight.constant = 0.0
        
        characterName.text = character.name
        characterDescription.text = character.description
        
        characterView.backgroundColor = UIColor(red: 45/255.0, green: 46/255.0, blue: 47/255.0, alpha: 1.0)
        
        if characterDescription.text != nil {
            if let descriptionSize = UIFont(name: "TrebuchetMS-Italic", size: 17.0)?.sizeOfString((characterDescription.text! as NSString), constrainedToWidth: Double(UIScreen.main.bounds.size.width - 16.0)) {
                if descriptionSize.height < 21.0 {
                    characterDescriptionHeight.constant = 21.0
                } else {
                    characterDescriptionHeight.constant = descriptionSize.height
                }
            }
        } else {
            characterDescriptionHeight.constant = -14.0
        }
        
        characterViewHeight.constant = 172.0 + characterDescriptionHeight.constant
        
        createCharacterButton()
        
        avatar.layer.cornerRadius = 50.0
        avatar.clipsToBounds = true
    }
    
    func toggleFavorite(_ sender: UIBarButtonItem) {
        if favorited {
            navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        } else {
            navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 255/255.0, green: 203/255.0, blue: 43/255.0, alpha: 1.0)
        }
        
        favorited = !favorited
        
        var saved: Bool = false
        
        if favorited {
            saved = CharacterModel.save(character)
        } else {
            saved = CharacterModel.delete(character)
        }
        
        if saved {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "ReloadFavorites"), object: nil)
        }
    }
    
    func createCharacterButton() {
        if let titleSize = UIFont(name: "TrebuchetMS", size: 20.0)?.sizeOfString((title! as NSString), constrainedToWidth: Double(view.frame.width - 100.0)) {
            let btnTitle: UIButton = UIButton(type: .custom)
            btnTitle.frame = CGRect(x: 0.0, y: 0.0, width: titleSize.width + 50.0, height: 44.0)
            btnTitle.titleLabel?.font = UIFont(name: "TrebuchetMS", size: 20.0)
            btnTitle.setTitle(title, for: UIControlState())
            btnTitle.setTitleColor(UIColor.white, for: UIControlState())
            btnTitle.setTitleColor(UIColor(white: 255/255.0, alpha: 0.6), for: .highlighted)
            btnTitle.setImage(UIImage.fontAwesomeIconWithName(.CaretDown, textColor: UIColor.white, size: CGSize(width: 20.0, height: 20.0)), for: .normal)
            btnTitle.setImage(UIImage.fontAwesomeIconWithName(.CaretDown, textColor: UIColor(white: 255/255.0, alpha: 0.6), size: CGSize(width: 20.0, height: 20.0)), for: .highlighted)
            
            btnTitle.adjustsImageWhenHighlighted = false
            
            btnTitle.imageEdgeInsets = UIEdgeInsets(top: (44.0 - 25.0), left: 0.0, bottom: 0.0, right: -titleSize.width)
            btnTitle.titleEdgeInsets = UIEdgeInsets(top: -(44.0 - titleSize.height - 10.0), left: 0.0, bottom: 0.0, right: titleSize.height)
            
            btnTitle.addTarget(self, action: #selector(DetailsViewController.toggleOptions(_:)), for: .touchUpInside)
            
            navigationItem.titleView = btnTitle
            
            if characterViewHeight.constant == 0.0 {
                btnTitle.imageView?.transform = CGAffineTransform.identity
            } else {
                btnTitle.imageView?.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if character.thumbnail != nil {
            let urlRequest = URLRequest(url: URL(string: character.thumbnail!)!)
            let filter = AspectScaledToFillSizeCircleFilter(size: CGSize(width: 100.0, height: 100.0))
            
            downloader.download(urlRequest, filter: filter) { response in
                if let image = response.result.value {
                    self.avatar.image = image
                }
            }
        }
        
        if events.count == 0 {
            loadMore()
        }
    }
    
    func toggleOptions(_ sender: UIButton) {
        if characterViewHeight.constant == 0.0 {
            characterViewHeight.constant = 172.0 + characterDescriptionHeight.constant
        } else {
            characterViewHeight.constant = 0.0
        }
        
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
            
            if self.characterViewHeight.constant == 172.0 + self.characterDescriptionHeight.constant {
                sender.imageView?.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
            } else {
                sender.imageView?.transform = CGAffineTransform.identity
            }
        })
    }
    
    func loadMore() {
        isLoading = true
        
        let characterWs: CharacterWs = CharacterWs(success: responseSuccess, failure: responseFailure)
        characterWs.fetchEvents(characterId: character.id, limit: limit, offset: offset)
    }
    
    func responseSuccess(_ data: Mappable?) {
        isLoading = false
        
        loadingIndicator?.stopAnimating()
        
        if let response = data as? GenericResponse<EventResponse> {
            if response.code == 200 {
                if let data = response.data {
                    events.append(contentsOf: data.results!)
                    
                    if (offset + 1) * limit >= data.total! {
                        hasMore = false
                    }
                }
            } else {
                Functions.showAlert(NSLocalizedString("Alert", comment: ""), message: response.status, buttonTitle: NSLocalizedString("Ok", comment: ""), viewController: self)
            }
        }
        
        collectionView.reloadData()
        
        if events.count == 0 {
            addEmptyOverlay()
        }
    }
    
    func responseFailure(_ error: NSError?) {
        isLoading = false
        
        if offset > 0 {
            offset -= limit
        }
        
        loadingIndicator?.stopAnimating()
        
        if error != nil {
            addEmptyOverlay()
            
            Functions.showAlert(NSLocalizedString("Alert", comment: ""), message: error!.localizedDescription, buttonTitle: NSLocalizedString("Ok", comment: ""), viewController: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if maximumOffset - currentOffset > 1.0 {
            return
        }
        
        if hasMore && isLoading == false {
            loadingIndicator?.startAnimating()
            
            offset += limit
            
            loadMore()
        }
    }

    // MARK: UICollectionViewDataSource

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return events.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdentifier", for: indexPath) as? EventViewCell {
            let imageView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: widthItem, height: widthItem))
            imageView.contentMode = .scaleAspectFill
            imageView.image = UIImage(named: "placeholder")
            
            cell.backgroundView = imageView
            
            let event = events[indexPath.row]
            
            if event.thumbnail != nil {
                if let path = event.thumbnail!.path, let type = event.thumbnail!.type {
                    let thumbnail = "\(path).\(type)".replacingOccurrences(of: "http://", with: "https://")
                    
                    let urlRequest = URLRequest(url: URL(string: thumbnail)!)
                    let filter = AspectScaledToFillSizeFilter(size: CGSize(width: widthItem, height: widthItem))
                    
                    downloader.download(urlRequest, filter: filter) { response in
                        if let image = response.result.value {
                            imageView.image = image
                        }
                    }
                }
            }
            
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let reusableView: UICollectionReusableView = UICollectionReusableView()
        
        if kind == UICollectionElementKindSectionFooter {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "footerView", for: indexPath)
            
            footerView.addSubview(loadingIndicator!)
            
            return footerView
        } else if kind == UICollectionElementKindSectionHeader {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerView", for: indexPath)
            
            if events.count > 0 {
                let lbHeader: UILabel = UILabel(frame: CGRect(x: 16.0, y: 16.0, width: 200.0, height: 21.0))
                
                lbHeader.text = NSLocalizedString("Events", comment: "").uppercased()
                lbHeader.textColor = UIColor.darkGray
                lbHeader.font = UIFont(name: "TrebuchetMS", size: 18.0)
                
                headerView.addSubview(lbHeader)
            }
            
            return headerView
        }
        
        return reusableView
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) {
            cell.contentView.backgroundColor = UIColor(white: 0/255.0, alpha: 0.4)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) {
            cell.contentView.backgroundColor = nil
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let event = events[indexPath.row]
        
        if let urls = event.urls {
            if urls.count > 0 {
                let actionSheet = UIAlertController(title: nil, message: NSLocalizedString("Open", comment: ""), preferredStyle: .actionSheet)
                
                for url: UrlResponse in urls {
                    actionSheet.addAction(UIAlertAction(title: url.type!, style: .default, handler: { (alert) in
                        UIApplication.shared.openURL(URL(string: url.url!)!)
                    }))
                }
                
                actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
                
                present(actionSheet, animated: true, completion: nil)
            }
        }
    }
    
    func addEmptyOverlay() {
        if collectionView.viewWithTag(1) != nil {
            removeEmptyOverlay()
        }
        
        let label = UILabel()
        
        label.backgroundColor = UIColor.white
        label.frame.size.height = 42
        label.frame.size.width = collectionView.frame.size.width
        label.center = collectionView.center
        label.center.y = collectionView.frame.size.height / 2
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textColor = UIColor.gray
        label.text = NSLocalizedString("No events found for this character.", comment: "")
        label.font = UIFont(name: "TrebuchetMS", size: 18.0)
        label.textAlignment = .center
        label.autoresizingMask = ([.flexibleBottomMargin, .flexibleTopMargin, .flexibleRightMargin, .flexibleLeftMargin])
        label.tag = 1
        label.alpha = 0.0
        
        label.translatesAutoresizingMaskIntoConstraints = false
        
        collectionView.addSubview(label)
        collectionView.isUserInteractionEnabled = false
        
        let horizontalConstraint = NSLayoutConstraint(item: label, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: collectionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0)
        collectionView.addConstraint(horizontalConstraint)
        
        let verticalConstraint = NSLayoutConstraint(item: label, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: collectionView, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0)
        collectionView.addConstraint(verticalConstraint)
        
        let views = ["label": label]
        
        let widthConstraints = NSLayoutConstraint.constraints(withVisualFormat: "|-[label]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        collectionView.addConstraints(widthConstraints)
        
        let heightConstraints = NSLayoutConstraint.constraints(withVisualFormat: "|-[label]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        collectionView.addConstraints(heightConstraints)
        
        UIView.animate(withDuration: 0.3, animations: {
            label.alpha = 1.0
        })
    }
    
    func removeEmptyOverlay() {
        if let content = collectionView.viewWithTag(1) {
            content.removeFromSuperview()
            collectionView.isUserInteractionEnabled = true
        }
    }

}
