//
//  Functions.swift
//  marvel
//
//  Created by Paulo Rodrigues on 28/09/16.
//  Copyright © 2016 Riabox Software. All rights reserved.
//

import Foundation
import MBProgressHUD

struct Functions {
    
    static func md5(_ string: String) -> String {
        let context = UnsafeMutablePointer<CC_MD5_CTX>.allocate(capacity: 1)
        var digest = Array<UInt8>(repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
        CC_MD5_Init(context)
        CC_MD5_Update(context, string, CC_LONG(string.lengthOfBytes(using: String.Encoding.utf8)))
        CC_MD5_Final(&digest, context)
        context.deallocate(capacity: 1)
        var hexString = ""
        for byte in digest {
            hexString += String(format:"%02x", byte)
        }
        
        return hexString
    }
    
    static func showIndeterminateHUD(_ label : String = "") -> () {
        let window = UIApplication.shared.keyWindow
        
        var hasHUD: Bool = false
        
        for view: Any in window!.subviews {
            if view is MBProgressHUD {
                hasHUD = true
                (view as! MBProgressHUD).labelText = label
                break
            }
        }
        
        if !hasHUD {
            let loadingHUD = MBProgressHUD.showAdded(to: window, animated: true)
            loadingHUD?.dimBackground = true
            loadingHUD?.mode = MBProgressHUDMode.indeterminate
        }
    }
    
    static func hideIndeterminateHUD() {
        let window = UIApplication.shared.keyWindow
        MBProgressHUD.hide(for: window, animated: true)
    }
    
    static func showAlert(_ title: String, message: String, buttonTitle: String, viewController: UIViewController) {
        let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: buttonTitle, style: .default) {(_) in}
        
        alert.addAction(okAction)
        
        viewController.present(alert, animated: true, completion: nil)
    }
    
    static func dateFromString(_ dateStr: String, format: String = "yyyy-MM-dd HH:mm:ss") -> Date? {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = format
        
        return dateFormatter.date(from: dateStr)
    }
    
    static func stringFromDate(_ date: Date, format: String = "yyyy-MM-dd HH:mm:ss") -> String? {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = format
        
        return dateFormatter.string(from: date)
    }
    
}
