//
//  UIFont+Utils.swift
//  marvel
//
//  Created by Paulo Rodrigues on 29/09/16.
//  Copyright © 2016 Riabox Software. All rights reserved.
//

import UIKit

extension UIFont {
    func sizeOfString (_ string: NSString, constrainedToWidth width: Double) -> CGSize {
        return string.boundingRect(with: CGSize(width: CGFloat(width), height: CGFloat.greatestFiniteMagnitude),
                                   options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                   attributes: [NSFontAttributeName: self],
                                   context: nil).size
    }
}
