//
//  marvelTests.swift
//  marvelTests
//
//  Created by Paulo Rodrigues on 28/09/16.
//  Copyright © 2016 Riabox Software. All rights reserved.
//

import XCTest
@testable import marvel

class marvelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInsertion() {
        let ironMan = Character(id: 1, name: "Iron Man")
        XCTAssertTrue(CharacterModel.save(ironMan))
        
        let spiderMan = Character(id: 2, name: "Spider-Man")
        XCTAssertTrue(CharacterModel.save(spiderMan))
        
        let captainAmerica = Character(id: 3, name: "Captain America")
        XCTAssertTrue(CharacterModel.save(captainAmerica))
    }
    
    func testFiding() {
        XCTAssertNotNil(CharacterModel.findById(id: 1))
    }
    
    func testDeletion() {
        let ironMan = Character(id: 1, name: "Iron Man")
        XCTAssertTrue(CharacterModel.delete(ironMan))
        
        let spiderMan = Character(id: 2, name: "Spider-Man")
        XCTAssertTrue(CharacterModel.delete(spiderMan))
        
        let captainAmerica = Character(id: 3, name: "Captain America")
        XCTAssertTrue(CharacterModel.delete(captainAmerica))
    }
    
    func testPerformanceListing() {
        self.measure {
            XCTAssertGreaterThan(CharacterModel.listAll().count, 0)
        }
    }
    
    func testFetchChatacters() {
        let expectation: XCTestExpectation = self.expectation(description: "Fetching characters")
        
        let characterWs: CharacterWs = CharacterWs(success: { (response) in
            let data = response as? GenericResponse<CharacterResponse>
            
            XCTAssertNotNil(data)
            XCTAssertEqual(data?.code, 200)
            
            expectation.fulfill()
            }) { (error) in
                XCTAssertNil(error)
                
                expectation.fulfill()
        }
        
        characterWs.fetchAll()
        
        self.waitForExpectations(timeout: 20.0) { (error) in
            XCTAssertNil(error)
        }
    }
    
}
